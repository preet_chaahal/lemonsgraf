<?php
require('lib/fpdf.php');

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',16);
$pdf->Cell(60,10,"First Name: ". $_REQUEST['first_name'], 0, 1);
$pdf->Cell(60,10,"Last Name: ". $_REQUEST['last_name'], 0, 1);
$pdf->Cell(60,10,"Email: ". $_REQUEST['email'], 0, 1);
$pdf->Cell(60,10,"Age: ". $_REQUEST['age'], 0, 1);
$pdf->Cell(60,10,"Gender: ". $_REQUEST['gender'], 0, 1);
$pdf->Cell(60,10,"Favourite Color: ". $_REQUEST['color'], 0, 1);
$pdf->Cell(60,10,"Address: ". $_REQUEST['address'], 0, 1);
$pdf->Output();
?>